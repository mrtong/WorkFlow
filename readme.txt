简介：
多数据库支持，同时支持：SQLServer，Mysql，Oracle，微软官方的WorkFlow入门要求比较高，所以我们萌生了开发一个简单的工作流引擎，帮助.Net coder们解决软件项目中流程的处理。 WikeFlow除了正常的流程处理，还支持流程动态跳转，我们的理念：写最少的代码，实现最炫酷的功能。框架上手容易，零难度。

项目配置：
1、初始化数据库在sql文件夹里面。

2、数据库连接在WikeSoft.Web/Web.config里面配置，本系统默认支持的是SQL

3、切换数据库要修改Web.config里面的三个地方
   a.<connectionStrings>节点
   b.<appSettings>下面的key='defaultSchema'
   c.<entityFramework>
