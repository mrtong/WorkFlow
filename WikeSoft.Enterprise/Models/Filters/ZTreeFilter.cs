﻿namespace WikeSoft.Data.Models.Filters
{
    /// <summary>
    /// 页面异步树过滤器
    /// </summary>
    public class ZTreeFilter
    {
        /// <summary>
        /// id
        /// </summary>
        public string id { get; set; }

        /// <summary>
        /// 层级
        /// </summary>
        public int level { get; set; }
    }
}
