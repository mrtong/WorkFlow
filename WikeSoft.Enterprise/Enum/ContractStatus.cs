﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WikeSoft.Data.Enum
{
    /// <summary>
    /// 合同状态
    /// </summary>
    public enum ContractStatusEnum
    {
        /// <summary>
        /// 能修改
        /// </summary>
        [Description("能修改")]
        UnValid = 0,
        /// <summary>
        /// 不能修改
        /// </summary>
        [Description("不能修改")]
        Valid = 1
    }
}
