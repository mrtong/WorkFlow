﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WikeSoft.Data.Enum
{
    /// <summary>
    /// 类别（0：待办，1：通知）
    /// </summary>
    public enum TaskWakeTypeEnum
    {
        [Description("待办")]
        WaitDo =0,

        [Description("通知")]
        Notice =1
    }
}
