﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WikeSoft.Data.Enum
{
    /// <summary>
    /// 归还状态
    /// </summary>
    public enum MaterialReturnStatusEnum
    {
        /// <summary>
        /// 申请归还
        /// </summary>
        [Description("申请归还")]
        ReturnApply = 0,

        /// <summary>
        ///已归还
        /// </summary>
        [Description("已归还")]
        ReturnAgree = 1,

        /// <summary>
        /// 归还被驳回
        /// </summary>
        [Description("归还被驳回")]
        ReturnReject = 2,
    }
}
