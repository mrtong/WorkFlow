﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WikeSoft.WorkFlowEngine.Enum
{
    public enum TaskAudit
    {
        /// <summary>
        /// 成功
        /// </summary>
        Agree = 100,
        /// <summary>
        /// 中止
        /// </summary>
        Suspend = 200,

    }
}
