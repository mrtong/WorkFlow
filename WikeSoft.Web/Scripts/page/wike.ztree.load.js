﻿//加载树状数据专用
var WikeTreeLoad = function (config) {
    this.id = config.id;
    this.setting = {
        view: {
            selectedMulti: false
        },
        async: {
            enable: true,
            url: config.loadUrl,
            autoParam: ["id", "name=n", "level=lv"]
        },
        callback: {
            onClick: config.onClick
        }
    };
};

WikeTreeLoad.prototype.load = function () {
    $.fn.zTree.init($("#" + this.id), this.setting);
}