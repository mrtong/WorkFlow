﻿/*
** 此js是添加，编辑页面调用
*/
(function () {
    $("button[action='submit'].btn").click(function () {
        var form = $("form:first");
        if (form.valid()) {
            $(this).button("loading");
            form.submit();
        }
        return false;
    });
    $("button[action='back'].btn").click(function () {
       
        var url = $(this).data("url");
        $(this).button("loading");
        window.location.href = url;
    });

    $("ul.nav-tabs li").click(function () {
        $(this).parent().find("li").removeClass("active");
        $(this).addClass("active");
        var id = "#" + $(this).attr("target");
        $(".tab-content").hide();
        $(id).show();
        $(window).resize();
    });

})();