﻿using System;
using System.Web.Mvc;

namespace WikeSoft.Web
{
    /// <summary>
    /// 忽略密码过期验证
    /// </summary>
    [Authorize]
    public class IgnorePasswordExpirationAttribute : Attribute
    {

    }

    
}